SHELL := /bin/bash

PWD=$(shell pwd)
STATUS=0
EXP_FILE=build/docs/runtime/public/bs-exp.html
PUB_DEST=build/code/runtime/public
API_DIR=build/code/runtime/api
PANE_DIR=build/code/runtime/pane-components
RUN_SRC_DEST=build/code/runtime/src
RUNTIME=build/code/runtime

all: build

init:
	./init.sh

build: init
	make -f pub-make -k build

build-with-basic-infra: init clone-basic-infra all
	echo "build with basic infra"

clone-basic-infra: init wget-orgs clone-exporters clone-basic-themes
	echo "infrastructure put in place"

clone-ere-infra: init clone-basic-infra clone-ere-repos
	echo "exp runtime cloned and built"

clone-basic-themes: init
	make -f pub-make -k clone-basic-themes

clone-ere-repos: init
	make -f pub-make -k clone-ere-repos

build-ere: clone-basic-infra clone-ere-repos
	make -f pub-make -k build-ere

mini-build-ere: clone-ere-repos
	make -f pub-make -k build-ere

publish-ere:
	make -f pub-make -k publish-ere

clone-exporters: init
	make -f pub-make -k clone-exporters

wget-orgs: init
	make -f pub-make -k wget-orgs

clean:
	make -f pub-make clean
	(rm -rf build; rm -rf ./ere-publisher)

clean-infra:
	make -f pub-make clean-infra
