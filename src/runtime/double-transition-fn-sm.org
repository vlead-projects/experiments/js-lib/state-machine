#+TITLE: State Machine with a two transition functions
#+AUTHOR: VLEAD
#+DATE: [2019-08-23 Fri]
#+SETUPFILE: ../org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
A state machine with a single transition function is built
here.


* Design


* Implementation
** Node Modules
#+BEGIN_SRC json :tangle sm2/package.json
{
  "name": "todos",
  "version": "0.0.1",
  "private": true,
  "devDependencies": {
    "react-scripts": "^3.0.0"
  },
  "dependencies": {
    "prop-types": "^15.7.2",
    "react": "^16.8.6",
    "react-dom": "^16.8.6",
    "react-redux": "^7.0.2",
    "redux": "^4.0.1"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "eject": "react-scripts eject",
    "test": "react-scripts test --env=node"
  },
  "browserslist": [
    ">0.2%",
    "not dead",
    "not ie <= 11",
    "not op_mini all"
  ]
}

#+END_SRC

** Public Html
#+BEGIN_SRC html :tangle sm2/public/index.html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Redux Todos Example</title>
  </head>
  <body>
    <div id="root"></div>
    <!--
      This HTML file is a template.
      If you open it directly in the browser, you will see an empty page.

      You can add webfonts, meta tags, or analytics to this file.
      The build step will place the bundled scripts into the <body> tag.

      To begin the development, run `npm start` in this folder.
      To create a production bundle, use `npm run build`.
    -->
  </body>
</html>


#+END_SRC
** Src
*** Index.js
#+BEGIN_SRC js :tangle sm2/src/index.js
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './components/App'
import { store } from './reducers'

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)


#+END_SRC
*** Actions
#+BEGIN_SRC js :tangle sm2/src/actions/index.js
export const incrInt = () => ({
  type: 'INCRINT'
})

export const incrAlpha = () => ({
  type: 'INCRALPHA'
})

#+END_SRC
*** Components
**** App
#+BEGIN_SRC js :tangle sm2/src/components/App.js
import React from 'react'
import LabelContainer from '../containers/LabelContainer'
import ButtonContainer from '../containers/ButtonContainer'
import LabelContainerA from '../containers/LabelContainerA'
import ButtonContainerA from '../containers/ButtonContainerA'

const App = () => (
  <div>
    <div>
      <LabelContainer />
      <ButtonContainer />
    </div>
    <div>
      <LabelContainerA />
      <ButtonContainerA />
    </div>
  </div>
)

export default App

#+END_SRC
**** Button
#+BEGIN_SRC js :tangle sm2/src/components/Button.js
import React from 'react'

const Button = (props) => (
  <button onClick={props.incrInt}> Click to Increment </button>
)

export default Button

#+END_SRC

**** ButtonA
#+BEGIN_SRC js :tangle sm2/src/components/ButtonA.js
import React from 'react'

const ButtonA = (props) => (
  <button onClick={props.incrAlpha}> Click for next Alphabet </button>
)

export default ButtonA

#+END_SRC

**** Label
#+BEGIN_SRC js :tangle sm2/src/components/Label.js
import React from 'react'

const Label = (props) => (
  <label> {props.value} </label>
)

export default Label

#+END_SRC
*** Containers
**** ButtonContainer
#+BEGIN_SRC js :tangle sm2/src/containers/ButtonContainer.js
import { connect } from 'react-redux';
import Button from '../components/Button';
import { incrInt } from '../actions';

const mapDispatchToProps = dispatch => ({
  incrInt: () => dispatch(incrInt())
});

export default connect(null, mapDispatchToProps)(Button);

#+END_SRC
**** LabelContainer
#+BEGIN_SRC js :tangle sm2/src/containers/LabelContainer.js
import React, { Component } from 'react';
import { store } from '../reducers'
import Label from '../components/Label';

class LabelContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      storeVal: 0
    };
  }
  
  updateInternalState = () => {
    let storeCtr = store.getState().intIncrEventCtr;
    if (storeCtr > this.state.storeVal) {
      let newCounter = this.state.value + 1;
      this.setState({value: newCounter, storeVal: storeCtr});
    }
  }
  
  componentDidMount() {
    this.unsubscribeStore = store.subscribe(this.updateInternalState);
    this.setState({value: 0, storeVal: store.getState().intIncrEventCtr});
  }
  
  componentWillUnmount() {
    this.unsubscribeStore();
  }
  
  render() {
    return (
      <Label value={this.state.value} />
    )
  }
}

export default LabelContainer;

#+END_SRC

**** ButtonContainerA
#+BEGIN_SRC js :tangle sm2/src/containers/ButtonContainerA.js
import { connect } from 'react-redux';
import ButtonA from '../components/ButtonA';
import { incrAlpha } from '../actions';

const mapDispatchToProps = dispatch => ({
  incrAlpha: () => dispatch(incrAlpha())
});

export default connect(null, mapDispatchToProps)(ButtonA);


#+END_SRC
**** LabelContainerA
#+BEGIN_SRC js :tangle sm2/src/containers/LabelContainerA.js
import React, { Component } from 'react';
import { store } from '../reducers'
import Label from '../components/Label';

class LabelContainerA extends Component {
  constructor(props) {
    super(props);
    this.dict = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E'};
    this.state = {
      value: 'A',
      storeVal: 0
    };
  }
  
  updateInternalState = () => {
    let storeCtr = store.getState().alphaIncrEventCtr;
    if (storeCtr > this.state.storeVal) {
      let alphabet = this.dict[(storeCtr) % 5];
      this.setState({value: alphabet, storeVal: storeCtr});
    }
  }
  
  componentDidMount() {
    this.unsubscribeStore = store.subscribe(this.updateInternalState);
    this.setState({value: 'A', storeVal: store.getState().alphaIncrEventCtr});
  }
  
  componentWillUnmount() {
    this.unsubscribeStore();
  }
  
  render() {
    return (
      <Label value={this.state.value} />
    )
  }
}

export default LabelContainerA;

#+END_SRC

*** Reducers
#+BEGIN_SRC index.js :tangle sm2/src/reducers/index.js
import { createStore } from 'redux'

const initialState = {
  intIncrEventCtr: 0,
  alphaIncrEventCtr: 0
}

function singleReducer(state = initialState, action) {
  switch (action.type) {
    case 'INCRINT':
      return {...state, intIncrEventCtr: state.intIncrEventCtr + 1};
    case 'INCRALPHA':
      return {...state, alphaIncrEventCtr: state.alphaIncrEventCtr + 1};
    default:
      return state;
  }
}

export const store = createStore(singleReducer)

#+END_SRC
